var app = angular.module('app', []);
app.filter('makeRange', function () {
    return function (input) {
        var lowBound, highBound;
        switch (input.length) {
            case 1:
                lowBound = 0;
                highBound = parseInt(input[0]) - 1;
                break;
            case 2:
                lowBound = parseInt(input[0]);
                highBound = parseInt(input[1]);
                break;
            default:
                return input;
        }
        var result = [];
        for (var i = lowBound; i <= highBound; i++)
            result.push(i);
        return result;
    };
});
            
var authenticationCredentials = {};
var authenticationMode = "ANONAUTH";

var authenticationStatus = "Please authenticate yourself.";

var currentPage = 'templates/section-showOrders.html';

app.controller("OrderDisplayController", function ($scope) {
    //Initialize default values for current controller
    $scope.query = "";
    $scope.queryInformation = "0 Results.";
    $scope.results = new CuzaPIE.Spreadsheet("empty");
    $scope.entriesPerPage = 25;
    $scope.page = 0;

    $scope.isLoading = false;
                
    $scope.refresh = function () {
        if (!$scope.isLoading) {
            $scope.isLoading = true;
            var currentRequest = new CuzaPIE.Request("/api.xml", "cartcompact.orders.query");
            //currentRequest = new CuzaPIE.Request("/tests/client/order.query.s01.xml", "cartcompact.query", true);

            //Validate and copy the parameters to the request
            currentRequest.parameters.searchValue = $scope.query.toString();
            currentRequest.parameters.entriesPerPage = Math.min(200, $scope.entriesPerPage);
            currentRequest.parameters.page = Math.max(0, $scope.page).toString();

            $scope.entriesPerPage = currentRequest.parameters.entriesPerPage;
            $scope.page = currentRequest.parameters.page;
            $scope.tableDisplay = [];
            $scope.tableDisplay["firstname"] = true;
            $scope.tableDisplay["lastname"] = true;
            $scope.tableDisplay["city"] = true;
            $scope.tableDisplay["redirected"] = true;
            $scope.tableDisplay["debit"] = true;
            $scope.tableDisplay["credit"] = true;

            //Send the request to the server and lock the UI meanwhile
            currentRequest.send(authenticationMode, authenticationCredentials,
                    function (response) {
                        $scope.results = response.output.orders;
                        $scope.queryInformation = response.output.status;
                        $scope.isLoading = false;
                        $scope.$apply();
                    }, function (error) {
                $scope.queryInformation = "Server error (" + error + ").";
                $scope.isLoading = false;
                $scope.$apply();
            });
        }
    };

    $scope.refresh();
});

var doLogout = function (message, handleAsError) {
    authenticationCredentials = {};
};


$(document).ready(function () {
    $("#formLogin").submit(function () {
        $("#sectionLogin").fadeOut(500, function () {
            $("#sectionMain").fadeIn(500);
        });
    });
    
    $("#sectionLogin").fadeIn(1000);
});
/*
 authenticationCredentials = {};
 authenticationCredentials.email = $("#loginInputEmail").val();
authenticationCredentials.password = $("#loginInputPassword").val();
         
//var loginRequest = CuzaPIE.Request("/api.xml", "");
Backoffice.authenticate(authenticationCredentials, function() {
    $("#formLogin").submit(function () {
        $("#sectionLogin").fadeOut(500, function () {
            $("#sectionMain").fadeIn(500);
    });
});
         },
         function() {
         doLogout();
         authenticationStatus = "Please try again."
         });
         return false;
            $(document).ready(function() {
$("#sectionLogin").fadeIn(1000);
});*/
