/*
 * Brevis Reloaded
 * Copyright (C) 2015, Prepaid24 GmbH
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function TeBow(divId) {

    this.divId = divId;
    this.testCases = [];
    this.position = 0;

    document.getElementById(this.divId).style = "text-align:center;";
    document.getElementById(this.divId).innerHTML = "<div id='panel' class='panel panel-default' style='margin: 20px; display: inline-block; text-align: center;'></div>";
    document.getElementById("panel").innerHTML = "<div class='panel-heading'>Tests und so</div><div id='panelBody' class='panel-body'></div>";
    document.getElementById("panelBody").innerHTML = "<div class='container-fluid'>" +
            "<div class='row'>" +
            "<div id='columns' class='col-md-12 column'></div>" +
            "</div>" +
            "<a id='start' class='btn btn-default pull-right'>Start</a>" +
            "</div>";

    document.getElementById("columns").innerHTML =
            "<table class='table table-bordered table-hover'" +
            "<thead ><tr class='active'><th class='text-center'>Execute</th><th class='text-center' style='width=100%'>Testcase</th><th class='text-center'>Result</th><th class='text-center'>Message</th></tr></thead>" +
            "<tbody id='testCases'></tbody>" +
            "</table>";


    /**
     * This function adds a test case to the boilerplate
     * @param {function} f
     * @param {string} name
     */
    this.addTest = function (f, name) {

        this.testCases[this.testCases.length] = {};
        this.testCases[this.testCases.length - 1].f = f;
        this.testCases[this.testCases.length - 1].name = name;


        var row = document.getElementById("testCases").insertRow(this.testCases.length - 1);

        var checkBox = row.insertCell(0);
        checkBox.innerHTML = "<div class='checkbox'><label><input id='check" + (this.testCases.length - 1) + "' type='checkbox' checked></label></div>";

        var testCell = row.insertCell(1);
        testCell.innerHTML = name;

        var resultCell = row.insertCell(2);
        resultCell.innerHTML = "<span class='glyphicon glyphicon-heart' aria-hidden='true'></span>";

        row.insertCell(3);
        //this.runTests(this.testCases);


    };

    this.runNextTest = function (t, pos) {
        t[pos].f();
    };

    this.invokeNextTest = function () {
        if (this.testCases.length > this.position) {
            this.runNextTest(this.testCases, this.position);
        } else {
            this.position = 0;
        }
    };

    this.addClickHandler = function (obj, button, pos) {
        button.addEventListener("click", function () {
            obj.runNextTest(obj.testCases, pos);
        });
    };

    this.wasLastTestSuccessful = function (alrighty, msg, response) {
        var newIcon = "";
        if (alrighty) {
            newIcon = "<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>";
        } else {
            newIcon = "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
        }
        var row = document.getElementById("testCases").rows[this.position];
        row.cells[2].innerHTML = newIcon;
        row.cells[3].innerHTML = msg;
        row.className = alrighty ? "success " : "danger";
        this.testCases[this.position].response = response;
        ++this.position;
    };

    this.sendRequest = function (tebow, request, auth, authParams) {
        var checked = document.getElementById("check" + this.position).checked;
        if (checked) {
            request.send(auth, authParams, function (response) {
                tebow.wasLastTestSuccessful(response.successful, response.message, response);
                tebow.invokeNextTest();
            }, function (errorMessage) {
                tebow.wasLastTestSuccessful(false, errorMessage);
                tebow.invokeNextTest();
            });
        } else {
            var row = document.getElementById("testCases").rows[this.position];
            row.className = "none";
            row.cells[2].innerHTML = "<span class='glyphicon glyphicon-heart' aria-hidden='true'></span>";
            row.cells[3].innerHTML = "";
            ++this.position;
            tebow.invokeNextTest();
        }
    };
    
    this.getTestResult = function (testName, valueName) {
        for (var i = 0; i < this.testCases.length; ++i) {
            if (this.testCases[i].f.name === testName) {
                var response = this.testCases[i].response;
                if (response !== null && response !== undefined) {
                    return response.output[valueName];
                } else {
                    return null;
                }
            }
        }
        
        return null;
    };

    //var btn = document.createElement("BUTTON");
    //btn.appendChild(document.createTextNode("Start"));
    //document.getElementById("panel").appendChild(btn);
    var btn = document.getElementById("start");
    this.addClickHandler(this, btn, 0);
    //btn.addEventListener("click", this.runTests, this.testCases);

}