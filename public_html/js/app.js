/* 
 * Copyright (C) 2015 cardcompact
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies', 'ui.bootstrap'])
        .config(config)
        .directive('footer', function () {
            return {
                restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
                replace: false,
                templateUrl: "directives/footer.html",
                controller: ['$scope', '$filter', function ($scope, $filter) {
                    // Your behaviour goes here :)
                }]
            }
        })
        .directive('header', function () {
            return {
                restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
                replace: false,
                templateUrl: "directives/header.html",
                controller: ['$scope', '$filter', function ($scope, $filter) {
                    // Your behaviour goes here :)
                }]
            }
        })
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'modules/home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'modules/login/login.view.html',
                controllerAs: 'vm'
            })

            .otherwise({ redirectTo: '/login' });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        /*$rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });*/
    }
    
    
}) ();

/*.directive('header', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element.
        replace: true,
        //scope: {user: '='}, // TODO user rights !!
        templateUrl: "js/directives/header.html",
        
    }
})*/