/* 
 * Copyright (C) 2016 cardcompact
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function () {
    var app = angular.module('tableApp', ['ngSanitize', 'ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.autoResize']);

    /**
    * Service to work with CuzaPIE.Spreadsheet. 
    * It maintains the current state of the data
    */
    app.service('TableSpreadsheet', function() {
        this.spreadsheet;
        
      /*  this.getSpreadsheet = function(){
            var outSpreadsheet = this.spreadsheet;
            var index = outSpreadsheet.rowsAmount();
            
            console.log(index);
            outSpreadsheet.removeRow(index-1);
            console.log(outSpreadsheet.toRowArray());
            console.log(this.spreadsheet.toRowArray());
            return outSpreadsheet;
        }*/
        /**
         * Save CuzaPIE.Spreadsheet
         * 
         * @param {CuzaPIE.Spreadsheet} table the spreadsheet table
         */
        this.setTable = function(table){
            this.spreadsheet = table;
        };
        
        
        
        /**
         * Returns CuzaPIE.Spreadsheet as array for ui.grid parametrs
         * 
         * @returns {Array} CuzaPIE.Spreadsheet as array
         */
        this.getTable = function() {      
            return this.spreadsheet.toRowArray();
        };
        
        /**
         * Returns definition alle colums for ui.grid parametrs.
         * Change the display settings of columns in the ui.grid here
         * 
         * @returns {Array} collums definition
         */
        this.getColDefs = function() {
            var colDefs = [];
            var cols = this.spreadsheet.columns();
            
            for(var i = 0; i < cols.length; i++) {
                colDefs.push({name:cols[i], enableColumnResizing: true});
            }
            
            return colDefs;
        };
        
        /**
         * Adds new row in CuzaPIE.Spreadsheet end
         * 
         * @param {type} rows array with rows. 
         * If funktion has no parametrs, adds empty row 
         * 
         * @returns {CuzaPIE.Spreadsheet}
         */
        this.addRow = function(rows) {
            if (arguments.length === 0) {
                this.spreadsheet.addRow();

                return this.spreadsheet;
            } else {
                this.spreadsheet.addRow(rows);

                return this.spreadsheet;
            }
        };
        
        /**
         * Adds empty row unter num
         * 
         * @param {int} num row nummer
         */
        this.addEmptyAfterRow = function(num){
            this.spreadsheet.addRowAt(this.getEmptyRow(), num);
            
            return this.spreadsheet;
        };
        
        /**
         * Generates empty row
         * 
         * @returns {Array} empty row
         */
        this.getEmptyRow = function(){
            var emptyRow = [];
            var cols = this.spreadsheet.columns();
            
            for(var i = 0; i < cols.length; i++) {
                emptyRow.push("");
            }
            
            return emptyRow;
        };
        
        /**
         * Remove row mit nummer num
         * 
         * @param {type} num row nummer
         * @returns {CuzaPIE.Spreadsheet}
         */
        this.removeRow = function(num) {
            this.spreadsheet.removeRow(num);
            
            console.log("Remove row " + num);
            console.log(this.spreadsheet.toRowArray());
            console.log(" ");
            
            return this.spreadsheet;
        };
        
        /**
         * Edit cell value
         * 
         * @param {string} value the value
         * @param {string} columnName column name
         * @param {int} rowIndex row nummer
         * @returns {CuzaPIE.Spreadsheet} 
         */
        this.editCell = function(value, columnName, rowIndex) {
            this.spreadsheet.insert(value, columnName, rowIndex);
            
            console.log("Edit cell");
            console.log(this.spreadsheet.toRowArray());
            console.log(" ");
            
            return this.spreadsheet;
        };
        
        return this;
    });
    
    /**
     * Describes the new HTML tag 'products-spreadsheet' with attribut 'table'.
     * Attribut 'table' transmits CuzaPIE.Spreadsheet
     */
    app.directive('productsSpreadsheet', function(){
        return {
            restrict: 'E',
            scope: {
                table: '='
            },
            controller: ['$scope','TableSpreadsheet', function($scope, TableSpreadsheet) {
                // Save Spreadsheet in service
                TableSpreadsheet.setTable($scope.table);
                
                var table = TableSpreadsheet.getTable();
                var columnDefs = TableSpreadsheet.getColDefs(); 
                columnDefs.push({name: 'Add',
                        cellTemplate: ''+
                            '<a class="btn primary" ng-click="grid.appScope.addRow(row)">'+
                            '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>'+
                            '</a> '+
                            '<a class="btn primary" ng-click="grid.appScope.deleteRow(row)">'+
                            '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span></a>',
                        enableCellEdit: false,
                        enableColumnMenu: false,
                        enableSorting: false,
                        enableHiding: false,
                        displayName: '', 
                        maxWidth: 85});
               /* columnDefs.push({name: 'Delete',
                        cellTemplate: '',
                        enableCellEdit: false,
                        enableColumnMenu: false,
                        enableSorting: false,
                        enableHiding: false,
                        displayName: '', 
                        maxWidth: 40});*/
                    
                $scope.gridOpts = {};
                $scope.gridOpts.columnDefs = columnDefs;
                $scope.gridOpts.data = table;
                $scope.gridOpts.enableHorizontalScrollbar = 0;
                $scope.gridOpts.enableVerticalScrollbar = 0;
                $scope.gridOpts.exporterMenuCsv = false;
                $scope.gridOpts.enableGridMenu = true;

                $scope.gridOpts.onRegisterApi = function(gridApi) {
                    //set gridApi on scope
                    $scope.gridApi = gridApi;
                    
                    /**
                     *  Raised when cell editing is complete 
                     *  @argument {object} rowEntity the options.data element that was edited
                     *  @argument {object} colDef the column that was edited  
                     *  @argument {object} newValue new value
                     *  @argument {object} oldValue old value
                     **/
                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                        if(newValue !== oldValue){
                            var rowIndex = $scope.gridOpts.data.indexOf(rowEntity) + 1;
                            var rowNum = $scope.gridOpts.data.length;

                            $scope.table = TableSpreadsheet.editCell(newValue, colDef.name, rowIndex);

                            //if the last line edited
                            if(rowIndex === rowNum){
                                $scope.table = TableSpreadsheet.addRow();
                                $scope.gridOpts.data = TableSpreadsheet.getTable();
                            }
                        }
                        $scope.$apply();
                    });
                };
                
                /**
                 * Function for Table auto resizing 
                 **/
                $scope.getTableHeight = function() {
                    var rowHeight = 30; // your row height
                    var headerHeight = 30; // your header height
                    return {
                       height: ($scope.gridOpts.data.length * rowHeight + headerHeight + 2) + "px"
                    };
                 };
  
                /**
                 * Adds new empty row under the current
                 * @param {int} row the current row
                 */
                $scope.addRow = function(row) {
                    var index = $scope.gridOpts.data.indexOf(row.entity) + 1;
                    var emptyRow = TableSpreadsheet.getEmptyRow();

                    $scope.table = TableSpreadsheet.addEmptyAfterRow(index);
                    $scope.gridOpts.data = TableSpreadsheet.getTable();
                    
                    
                };
                
                /**
                 * Delets the current row 
                 * @param {int} row the current row
                 */
                $scope.deleteRow = function(row) {
                    var index = $scope.gridOpts.data.indexOf(row.entity);
                    var rowNum = $scope.gridOpts.data.length;
                    
                    $scope.table = TableSpreadsheet.removeRow(index);
                    $scope.gridOpts.data = TableSpreadsheet.getTable();
                };
            }],
            templateUrl: 'directives/products-spreadsheet.html'
        };
    });
    
}) ();